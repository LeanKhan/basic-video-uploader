require("dotenv").config();
// Here we are!
const express = require("express");
const upload_router = require("./routes/upload");
const watch_router = require("./routes/watch");
const cors = require("cors");
const path = require("path");
// const mime = require("mime");
const app = express();

const environment = process.env.NODE_ENV;
const host = require("./config")[environment]["host"];
// Middleware

app.use(cors());

// routes

// Public files

app.use("/public", express.static(path.join(__dirname, "public")));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/page.html");
});
app.use("/media", upload_router);
app.use("/watch", watch_router);

app.get("/download/:filename", function (req, res) {
  const filename = req.params.filename;
  const file = `./media/${filename}`;

  res.download(file);
});

// Server

app.listen(5600, host, () => {
  console.log(`File Server started at port ${host}:5600`);
});
