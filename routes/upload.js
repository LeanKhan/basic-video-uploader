const upload_router = require("express").Router();
const fs = require("fs");
const formidable = require("formidable");
const ffmpeg = require("../controllers/ffmpeg");

// Routes

upload_router.route("/upload/:name").post((req, res) => {
  var response = {};
  var form = formidable.IncomingForm();

  //Change default maxFileSize

  form.maxFileSize = 800 * 1024 * 1024;

  form.multiples = false;

  form.encoding = "UTF-8";

  form.parse(req, function(err, fields, file) {
    name = fields.file_name;
  });

  var name = req["params"].name;

  var writable = fs.createWriteStream(`./media/${name}.exe`, {
    flags: "wx",
    autoClose: true
  });

  // form.onPart = function(part) {
  //   if (!part.filename) {
  //     form.handlePart(part);
  //   } else if (part.filename.match(/\.(mov|mkv|avi|mp4)$/i)) {
  //     console.log("File Upload Started");
  //     part.pipe(writable);
  //     ffmpeg.thumbnail(writable.path);
  //   } else {
  //     console.log("Not a supported file format!");
  //   }
  // };

  form.onPart = function(part) {
    if (!part.filename) {
      form.handlePart(part);
    } else if (part.filename.match(/\.*/i)) {
      console.log("File Upload Started");
      part.pipe(writable);
      // ffmpeg.thumbnail(writable.path);
    } else {
      console.log("Not a supported file format!");
    }
  };

  form;
  // .on("progress", (br, be) => {
  //   console.log("Bytes received. " + br + ".Bytes expected" + be);
  // })
  // .on("end", () => {
  //   response.metadata = ffmpeg.metadata(writable.path);
  // });
  writable.on("finish", () => {
    // let metadata = ffmpeg.metadata(writable.path);
    // console.log(metadata);
    fs.stat(writable.path, (err, stats) => {
      if (err) throw err;
      if (stats) {
        response.stats = stats;
        response.message = "File Uploaded Successfully";
      }

      res.send(response).status(200);
    });

    // res.redirect('/');
  });
  writable.on("error", err => {
    if (err) {
      res.send(err);
      console.log(err);
      writable.close();
    }
  });
});

// Functions

module.exports = upload_router;
