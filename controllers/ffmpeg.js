// @LeanKhan

/**
 * * To use this file you need to install ffmpeg and ffprobe
 * on your machine
 * @see http://www.ffmpeg.org/download.html
 *
 * * Also you need to install fluent-ffmpeg form npm
 *
 * @description npm install fluent-ffmpeg
 *
 * * Documentation for the npm package
 * @see https://github.com/fluent-ffmpeg/node-fluent-ffmpeg
 *
 *
 * */
const ffmpeg = require("fluent-ffmpeg");

/**
 * ffmpeg probe file path
 *
 * path to ffprobe.exe in your computer
 */
ffmpeg.setFfprobePath(process.env.FFPROBE_PATH.trim());

/**
 * ffmpeg file path
 *
 * path to ffmpeg.exe in your computer
 *
 */
ffmpeg.setFfmpegPath(process.env.FFMPEG_PATH.trim());

module.exports = {
  /**
   * Metadata function
   *
   * Put the path to the video you want metadata
   * information for...
   * @param {*} video_path
   * @returns video metadata
   */
  metadata: function (video_path) {
    ffmpeg.ffprobe(video_path, (err, metadata) => {
      if (err) {
        console.log(err);
      } else {
        console.log(metadata);
        return metadata;
      }
    });
  },
  /**
   * thumbnail function
   *
   * @param {*} video_path
   * @returns 3 video thumbnails
   */
  thumbnail: function (video_path) {
    ffmpeg(video_path)
      .screenshots({
        count: 3,
        filename: "%f-thumbnail-%i.png",
        get folder() {
          return "./public/screenshots/" + video_path.split("/").pop();
        },
      })
      .on("error", (err) => {
        // console.log("An Error occured", err);
      })
      .on("filenames", function (filenames) {
        // console.log("Generated " + filenames.join(","));
      })
      .on("end", function () {
        // console.log("Screenshots produced!");
      });
  },
};

// const thumbnail = function(video_path) {
//   ffmpeg(video_path)
//     .on("error", err => {
//       console.log("An Error occured", err);
//     })
//     .on("end", function() {
//       console.log("Screenshots produced!");
//     })
//     .screenshots({
//       count: 3,
//       filename: "%f-thumbnail.png",
//       get folder() {
//         return "./thumbnails/" + video_path;
//       },
//       size: "640x480"
//     })
//     .on("filenames", function(filenames) {
//       console.log("Generated " + filenames.join(","));
//     });
// };

// thumbnail("movie.mp4");
